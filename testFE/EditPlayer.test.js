import { expect, jest, test, it, describe } from '@jest/globals';
global.jest = jest;


const EditPlayer = require('../components/EditPlayer.js');

describe('arrayContaining', () => {
  const expected = ['button'];
  it('matches even if received contains additional elements', () => {
    expect(['button']).toEqual(expect.arrayContaining(expected));
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['blockbutton']).not.toEqual(expect.arrayContaining(expected));
  });
});

describe('stringMatching in arrayContaining', () => {
  const expected = [
    expect.stringMatching(/^username/),
    expect.stringMatching(/^email/),
  ];
  it('matches even if received contains additional elements', () => {
    expect(['username', 'email', 'city']).toEqual(
      expect.arrayContaining(expected),
    );
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['username', 'city']).not.toEqual(
      expect.arrayContaining(expected),
    );
  });
});

test('resolves to title button', async () => {
  await expect(Promise.resolve('title')).resolves.toBe('title');
  await expect(Promise.resolve('button')).resolves.not.toBe('footer');
});

describe('not.stringContaining', () => {
  const expected = 'username';

  it('matches if the received value does not contain the expected substring', () => {
    expect('address').toEqual(expect.not.stringContaining(expected));
  });
});

