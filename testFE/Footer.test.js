import { expect, jest, test, it, describe } from '@jest/globals';
global.jest = jest;


const Footer = require('../components/Footer.js');

describe('arrayContaining', () => {
  const expected = ['icon'];
  it('matches even if received contains additional elements', () => {
    expect(['icon']).toEqual(expect.arrayContaining(expected));
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['blankicon']).not.toEqual(expect.arrayContaining(expected));
  });
});


describe('stringMatching in arrayContaining', () => {
  const expected = [
    expect.stringMatching(/^share/),
    expect.stringMatching(/^button/),
  ];
  it('matches even if received contains additional elements', () => {
    expect(['share', 'button', 'menu']).toEqual(
      expect.arrayContaining(expected),
    );
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['share', 'menu']).not.toEqual(
      expect.arrayContaining(expected),
    );
  });
});

test('resolves to footer', async () => {
  await expect(Promise.resolve('icon')).resolves.toBe('icon');
  await expect(Promise.resolve('button')).resolves.not.toBe('menu');
});

describe('not.stringContaining', () => {
  const expected = 'share';

  it('matches if the received value does not contain the expected substring', () => {
    expect('download').toEqual(expect.not.stringContaining(expected));
  });
});



