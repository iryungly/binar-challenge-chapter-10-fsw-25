import { expect, jest, test, it, describe } from '@jest/globals';
global.jest = jest;



const Navigationbar = require('../components/Navigationbar.js');

describe('arrayContaining', () => {
  const expected = ['nav'];
  it('matches even if received contains additional elements', () => {
    expect(['nav']).toEqual(expect.arrayContaining(expected));
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['blocknav']).not.toEqual(expect.arrayContaining(expected));
  });
});

describe('stringMatching in arrayContaining', () => {
  const expected = [
    expect.stringMatching(/^style/),
    expect.stringMatching(/^nav/),
  ];
  it('matches even if received contains additional elements', () => {
    // eslint-disable-next-line indent
      expect(['style', 'nav', 'middle']).toEqual(
      expect.arrayContaining(expected),
    );
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['style', 'middle']).not.toEqual(
      expect.arrayContaining(expected),
    );
  });
});

test('resolves to style', async () => {
  await expect(Promise.resolve('style')).resolves.toBe('style');
  await expect(Promise.resolve('style')).resolves.not.toBe('study');
});

describe('not.stringContaining', () => {
  const expected = 'style';

  it('matches if the received value does not contain the expected substring', () => {
    expect('Study').toEqual(expect.not.stringContaining(expected));
  });
});
