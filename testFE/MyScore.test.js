import { expect, jest, test, it, describe } from '@jest/globals';
global.jest = jest;



const MyScore = require('../components/MyScore.js');

describe('arrayContaining', () => {
  const expected = ['background'];
  it('matches even if received contains additional elements', () => {
    expect(['background']).toEqual(expect.arrayContaining(expected));
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['blockbackground']).not.toEqual(expect.arrayContaining(expected));
  });
});

describe('stringMatching in arrayContaining', () => {
  const expected = [
    expect.stringMatching(/^email/),
    expect.stringMatching(/^score/),
  ];
  it('matches even if received contains additional elements', () => {
    expect(['email', 'score', 'address']).toEqual(
      expect.arrayContaining(expected),
    );
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['email', 'address']).not.toEqual(
      expect.arrayContaining(expected),
    );
  });
});

test('resolves to setemail', async () => {
  await expect(Promise.resolve('setemail')).resolves.toBe('setemail');
  await expect(Promise.resolve('setemail')).resolves.not.toBe('setpassword');
});


describe('not.stringContaining', () => {
  const expected = 'score';

  it('matches if the received value does not contain the expected substring', () => {
    expect('prolog').toEqual(expect.not.stringContaining(expected));
  });
});

