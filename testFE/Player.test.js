import { expect, jest, test, it, describe } from '@jest/globals';
global.jest = jest;


const Player = require('../components/Player.js');

describe('arrayContaining', () => {
  const expected = ['font'];
  it('matches even if received contains additional elements', () => {
    expect(['font']).toEqual(expect.arrayContaining(expected));
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['blockfont']).not.toEqual(expect.arrayContaining(expected));
  });
});

describe('stringMatching in arrayContaining', () => {
  const expected = [
    expect.stringMatching(/^text/),
    expect.stringMatching(/^center/),
  ];
  it('matches even if received contains additional elements', () => {
    expect(['text', 'center', 'padding']).toEqual(
      expect.arrayContaining(expected),
    );
  });
  it('does not match if received does not contain expected elements', () => {
    expect(['center', 'padding']).not.toEqual(
      expect.arrayContaining(expected),
    );
  });
});

test('resolves to font-semibold', async () => {
  await expect(Promise.resolve('font-semibold')).resolves.toBe('font-semibold');
  await expect(Promise.resolve('font-semibold')).resolves.not.toBe('font-medium');
});

function player() {
  return null;
}
test('player returns null', () => {
  expect(player()).toBeNull();
});


