import { render, screen } from '@testing-library/react';
import { expect, jest, test } from '@jest/globals';
global.jest = jest;

// eslint-disable-next-line no-use-before-define
const Auth = require('../config/Auth.js');
// eslint-disable-next-line no-undef
describe("Index page", () => {
  // eslint-disable-next-line no-undef
  it("should render", () => {
  });
});

// eslint-disable-next-line no-undef
it("should render", () => {
  render(<button />);
  const button = screen.getByRole("button");
});

test('resolves to title button', async () => {
  await expect(Promise.resolve('title')).resolves.toBe('title');
  await expect(Promise.resolve('button')).resolves.not.toBe('footer');
});

// eslint-disable-next-line no-undef
describe('stringMatching in arrayContaining', () => {
  const expected = [
    expect.stringMatching(/^password/),
    expect.stringMatching(/^email/),
  ];
  // eslint-disable-next-line no-undef
  it('does not match if received does not contain expected elements', () => {
    expect(['email', 'city']).not.toEqual(
      expect.arrayContaining(expected),
    );
  });

  // eslint-disable-next-line no-undef
  describe('arrayContaining', () => {
    const expected = ['button'];
    // eslint-disable-next-line no-undef
    it('does not match if received does not contain expected elements', () => {
      expect(['blockbutton']).not.toEqual(expect.arrayContaining(expected));
    });
  });
});


