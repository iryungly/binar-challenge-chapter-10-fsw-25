import { expect, jest, test } from '@jest/globals';
global.jest = jest;


const AddPlayer = require('../components/AddPlayer.js');

// eslint-disable-next-line no-undef
describe('arrayContaining', () => {
  const expected = ['form'];
  // eslint-disable-next-line no-undef
  it('matches even if received contains additional elements', () => {
    expect(['form']).toEqual(expect.arrayContaining(expected));
  });
  // eslint-disable-next-line no-undef
  it('does not match if received does not contain expected elements', () => {
    expect(['blockform']).not.toEqual(expect.arrayContaining(expected));
  });
});


// eslint-disable-next-line no-undef
describe('stringMatching in arrayContaining', () => {
  const expected = [
    expect.stringMatching(/^username/),
    expect.stringMatching(/^email/),
  ];
  // eslint-disable-next-line no-undef
  it('matches even if received contains additional elements', () => {
    expect(['username', 'email', 'city']).toEqual(
      expect.arrayContaining(expected),
    );
  });
  // eslint-disable-next-line no-undef
  it('does not match if received does not contain expected elements', () => {
    expect(['username', 'city']).not.toEqual(
      expect.arrayContaining(expected),
    );
  });
});

test('resolves to title button', async () => {
  await expect(Promise.resolve('title')).resolves.toBe('title');
  await expect(Promise.resolve('button')).resolves.not.toBe('footer');
});

// eslint-disable-next-line no-undef
describe('not.stringContaining', () => {
  const expected = 'username';

  // eslint-disable-next-line no-undef
  it('matches if the received value does not contain the expected substring', () => {
    expect('address').toEqual(expect.not.stringContaining(expected));
  });
});

