// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAo_bSnT7xCQ5aV2o7q3eRK4Ki8_QnvHXc",
  authDomain: "binarchapter9.firebaseapp.com",
  projectId: "binarchapter9",
  storageBucket: "binarchapter9.appspot.com",
  messagingSenderId: "172015972682",
  appId: "1:172015972682:web:16a96cfaa2081d1f4f91b3",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// export const analytics = getAnalytics(app);
export default app;
export const auth = getAuth(app);
