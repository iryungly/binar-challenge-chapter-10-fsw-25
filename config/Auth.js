import Footer from "../components/Footer";
import style from "../assets/styles/login.module.css";
import { useState } from "react";
import { useRouter } from "next/router";
import image from "next/image";
import {
  signInWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithPopup,
} from "firebase/auth";
import { auth } from "../config/firebaseInit";
// import { GoogleIcon } from '../../public/image/google.png';

export default function Login() {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [buttonText, setButtonText] = useState("Sign In");

  const handleLogin = async (e) => {
    e.preventDefault();
    setButtonText("Loading...");
    try {
      await signInWithEmailAndPassword(auth, email, password);
      router.push("/gamelist");
      alert("login successful");
    } catch (error) {
      setError(error.message);
      setButtonText("Sign In");
    }
  };

  const provider = new GoogleAuthProvider();

  const handleGoogleLogin = async () => {
    try {
      await signInWithPopup(auth, provider);
      router.push("/gamelist");
      alert("login successful");
    } catch (error) {
      setError(error.message);
    }
  };

  const forgotPassword = () => {
    try {
      router.push("/forgot");
    } catch (error) {
      setError(error.message);
    }
  };

  return (
    <div>
      <div
        className="container mt-5"
        style={{
          paddingTop: "100px",
          marginBottom: "100px",
          backgroundColor: "purple",
        }}
      >
        <h2 className="text-center mb-4" style={{ color: "white" }}>
          Sign In
        </h2>
        {error && <p className="text-danger">{error}</p>}
        <form onSubmit={handleLogin} className={style.form}>
          <div className={style.formgroup}>
            <label htmlFor="email" className={style.label}>
              Email
            </label>
            <input
              type="email"
              className={style.input}
              id="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className={style.formgroup}>
            <label htmlFor="password" className={style.label}>
              Password
            </label>
            <input
              type="password"
              className={style.input}
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <button type="submit" className={`${style.button} btn-block`}>
            {buttonText}
          </button>
          <image src={require("../../public/image/google.png")} alt="" />
          <button
            onClick={handleGoogleLogin}
            className={`${style.button} btn-block`}
          >
            Sign in with Google
          </button>
          <p className={`${style.p} pt-3`} onClick={forgotPassword}>
            forgot password
          </p>
        </form>
        <br />
        <br />
        <br />
      </div>
      <Footer />
    </div>
  );
}
