import React from "react";
import ReactPlayer from "react-player";

class AudioPlayer extends React.Component {
  render() {
    return (
      <div>
        <audio controls src={this.props.audioUrl} />
      </div>
    );
  }
}

export default AudioPlayer;
