import React, { useState, useEffect } from "react";
import { Navbar, Container, Nav, NavLink } from "react-bootstrap";
import { useCookies } from "react-cookie";
import jwt from "jwt-decode";
import Image from "next/image";
import Link from "next/link";

const NavBar = () => {
  const [cookies, setCookies, deleteCookies] = useCookies();

  const [registerBtnDisplay, setRegisterBtnDisplay] = useState("flex");
  const [logInBtnDisplay, setLogInBtnDisplay] = useState("flex");
  const [profileBtnDisplay, setProfileBtnDisplay] = useState("flex");
  const [Username, setUsername] = useState("username");
  const [LogInOut, setLogInOut] = useState("Login");
  const [LogInOutStateState, setLogInOutStateState] = useState("blm_login");
  const [ProfileImage, setProfileImage] = useState("blankpp.webp");

  function loginout() {
    if (LogInOutStateState === "blm_login") {
      window.location.href = "/login";
    } else {
      deleteCookies("tgtoken");
      deleteCookies("googlelogin");
      window.location.href = "/";
    }
  }
  return (
    <Navbar collapseOnSelect expand="lg" variant="dark" id="navigations">
      <Container fluid>
        <Navbar.Brand href="/">
          <Image src="/game-logo.png" alt="" width={10} height={45} />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="nav-left">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/gamelist">Games</Nav.Link>
            <Nav.Link href="/about">About Us</Nav.Link>
            <Nav.Link href="/blog">Blog</Nav.Link>
            <Nav.Link href="/contact">Contact</Nav.Link>
            <Nav.Link
              href="/profile"
              style={{
                display: profileBtnDisplay,
                alignItems: "center",
                marginRight: "25px",
              }}
            >
              {Username}
            </Nav.Link>
          </Nav>
          <Link href="/register" style={{ display: registerBtnDisplay }}>
            <button className="play-btn">Register</button>
          </Link>
          <Link href="/login" style={{ display: logInBtnDisplay }}>
            <button className="play-btn">Login</button>
          </Link>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavBar;
