import React from 'react';
import ReactPlayer from 'react-player';

class VideoPlayer extends React.Component {
  render() {
    return (
      <ReactPlayer
        url={this.props.videoUrl}
        controls={true}
        width='100%'
        height='100%'
      />
    );
  }
}

export default VideoPlayer;
