import "../../assets/styles/index.module.css";
import Player from "../../components/Player";
import { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import AddPlayer from "../../components/AddPlayer";
import EditPlayer from "../../components/EditPlayer";
import NavBar from "../../components/Navigationbar";

function Updateuser() {
  const [players, setPlayers] = useState([
    {
      id: 1,
      username: "adminbinar",
      email: "adminbinar@gmail.com",
      firstname: "admin",
      lastname: "binar",
    },
    {
      id: 2,
      username: "sylvia",
      email: "sylvia@gmail.com",
      firstname: "syl",
      lastname: "via",
    },
  ]);

  function updatePlayer(id, newUsername, newEmail, newFirstname, newLastname) {
    const updatedPlayers = players.map((player) => {
      if (id === player.id) {
        return {
          ...player,
          username: newUsername,
          email: newEmail,
          firstname: newFirstname,
          lastname: newLastname,
        };
      }

      return player;
    });
    setPlayers(updatedPlayers);
  }

  function newPlayer(username, email, firstname, lastname) {
    const newPlayer = {
      id: uuidv4(),
      username,
      email,
      firstname,
      lastname,
    };
    setPlayers([...players, newPlayer]);
  }

  const showPlayers = true;
  return (
    <div className="">
      {showPlayers ? (
        <>
          <NavBar />
          <div
            style={{ textAlign: "left", backgroundColor: "black" }}
            className="flex flex-wrap justify-center"
          >
            <h3> Data Player: Update/Edit </h3>
            {players.map((player) => {
              const editPlayer = (
                <EditPlayer
                  id={player.id}
                  username={player.username}
                  email={player.email}
                  firstname={player.firstname}
                  lastname={player.lastname}
                  updatePlayer={updatePlayer}
                />
              );
              return (
                <Player
                  key={player.id}
                  id={player.id}
                  username={player.username}
                  email={player.email}
                  firstname={player.firstname}
                  lastname={player.lastname}
                  editPlayer={editPlayer}
                />
              );
            })}
          </div>
          <AddPlayer newPlayer={newPlayer} />
        </>
      ) : (
        <p>You cannot see the players</p>
      )}
    </div>
  );
}

export default Updateuser;
