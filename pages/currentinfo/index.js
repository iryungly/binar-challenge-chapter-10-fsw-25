import { getDatabase, ref, onValue } from "firebase/database";
import firebaseInit from "config/firebaseInit";
import { useEffect, useState } from "react";
import NavigationBar from "../../components/Navigationbar";
import Footer from "../../components/Footer";

const Currentinfo = () => {
  const [userstory, setUser] = useState([]);

  useEffect(() => {
    const db = getDatabase(firebaseInit);
    const dbRef = ref(db, "userstory");
    onValue(dbRef, (snapshot) => {
      setUser(snapshot.val());
    });
  }, []);

  return (
    <>
      <NavigationBar />
      <div className="board">
        <h1 className="leaderboard">Currentinfo</h1>

        <div id="profile">
          <div class="card">
            <div className="flex">
              <div className="flex">
                <div className="info">
                  <table className="table">
                    <tr>
                      <th>USERNAME</th>
                    </tr>
                    <br />
                  </table>
                  {userstory &&
                    userstory.map((userstory, key) => {
                      return (
                        <table class="table" key="">
                          <tr key={key}>
                            <td> {userstory.username}</td>
                          </tr>
                          <br />
                        </table>
                      );
                    })}
                </div>
              </div>
              <div className="info">
                <table className="table">
                  <tr>
                    <th>WIN</th>
                  </tr>
                  <br />
                </table>
                {userstory &&
                  userstory.map((userstory, key) => {
                    return (
                      <table class="table" key="">
                        <tr key={key}>
                          <td> {userstory.win}</td>
                        </tr>
                        <br />
                      </table>
                    );
                  })}
              </div>
              <div className="info">
                <table className="table">
                  <tr>
                    <th>DRAW</th>
                  </tr>
                  <br />
                </table>
                {userstory &&
                  userstory.map((userstory, key) => {
                    return (
                      <table class="table" key="">
                        <tr key={key}>
                          <td> {userstory.draw}</td>
                        </tr>
                        <br />
                      </table>
                    );
                  })}
              </div>
              <div className="info">
                <table className="table">
                  <tr>
                    <th>LOSE</th>
                  </tr>
                  <br />
                </table>
                {userstory &&
                  userstory.map((userstory, key) => {
                    return (
                      <table className="table" key="">
                        <tr key={key}>
                          <td> {userstory.lose}</td>
                        </tr>
                        <br />
                      </table>
                    );
                  })}
              </div>
              <div className="info">
                <table className="table">
                  <tr>
                    <th>RONDE</th>
                  </tr>
                  <br />
                </table>
                {userstory &&
                  userstory.map((userstory, key) => {
                    return (
                      <table class="table" key="">
                        <tr key={key}>
                          <td> {userstory.ronde}</td>
                        </tr>
                        <br />
                      </table>
                    );
                  })}
              </div>
              <div className="info">
                <table className="table">
                  <tr>
                    <th>SCORE</th>
                  </tr>
                  <br />
                </table>
                {userstory &&
                  userstory.map((userstory, key) => {
                    return (
                      <table class="table" key="">
                        <tr key={key}>
                          <td> {userstory.score}</td>
                        </tr>
                        <br />
                      </table>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Currentinfo;
