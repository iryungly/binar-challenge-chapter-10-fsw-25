// import NavigationBar from "../../components/Navigationbar";
import Footer from "../../components/Footer";

import { useState } from "react";
import { useRouter } from "next/router";
import { auth } from "../../config/firebaseInit";
import { sendPasswordResetEmail } from "firebase/auth";
import style from "../../assets/styles/login.module.css";
// import { Router } from "react-router";

export default function Forgot() {
  const router = useRouter();

  const [resetError, setResetError] = useState(null);
  const [resetSuccess, setResetSuccess] = useState(null);
  const [resetEmail, setResetEmail] = useState("");
  const [buttonText, setButtonText] = useState("Send Password reset email");

  const handleResetPassword = async (e) => {
    e.preventDefault();
    setButtonText("Loading...");
    try {
      await sendPasswordResetEmail(auth, resetEmail);
      setResetSuccess(
        `Password reset email sent to ${resetEmail}, redirecting to login page...`
      );
      setResetError(null);
      setTimeout(() => {
        router.push("/login");
      }, 5000);
    } catch (error) {
      setResetError(error.message);
      setResetSuccess(null);
      setButtonText("Send Password reset email");
    }
  };

  return (
    <div>
      <h2
        className="text-center mb-4"
        style={{ marginTop: "100px", color: "purple" }}
      >
        Reset Password
      </h2>
      <form
        onSubmit={handleResetPassword}
        className={style.form}
        style={{ marginTop: "10px", marginBottom: "100px" }}
      >
        <div className={style.formgroup}>
          <label htmlFor="reset-email" className={style.label}>
            Email
          </label>
          <input
            type="email"
            className={style.input}
            id="reset-email"
            value={resetEmail}
            onChange={(e) => setResetEmail(e.target.value)}
          />
        </div>
        <button type="submit" className={`${style.button} btn-block`}>
          {buttonText}
        </button>
        {resetSuccess && <p className="pt-3">{resetSuccess}</p>}
        {resetError && <p className="text-danger pt-3">{resetError}</p>}
      </form>
      <Footer />
    </div>
  );
}
