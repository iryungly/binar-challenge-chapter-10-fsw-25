import "bootstrap/dist/css/bootstrap.css";
import "../assets/styles/globals.css";
import Script from "next/script";
import Head from "next/head";
import "../assets/styles/main.css";
import "../assets/styles/register.css";
import "assets/styles/Leaderboard.css";
import "assets/styles/Navbar.css";
import { Provider } from "react-redux";
import { store } from "../src/index";

import type { AppProps } from "next/app";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
        crossOrigin="anonymous"
      />
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
}
