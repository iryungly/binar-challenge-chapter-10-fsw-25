import React from "react";
import NavBar from "../../components/Navigationbar";
import { Cloudinary } from "@cloudinary/url-gen";
import { AdvancedImage, AdvancedVideo } from "@cloudinary/react";
import { fill } from "@cloudinary/url-gen/actions/resize";

const Pdfmedia = () => {
  // Create a Cloudinary  cloud name.
  const cld = new Cloudinary({
    cloud: {
      cloudName: "dfi862ezx",
    },
  });
  // Instantiate a CloudinaryImage /video
  const myImage = cld.image("Binar/landingpage-1_viuj1l");
  const myVideo = cld.video("Binar/nature_q0xfow");

  // Resize
  myImage.resize(fill().width(1500).height(100));
  myVideo.resize(fill().width(1500).height(400));

  // Render the image in a React component.
  return (
    <>
      <NavBar />
      <div>
        <AdvancedImage cldImg={myImage} />
        <AdvancedVideo cldVid={myVideo} controls />
      </div>
    </>
  );
};
export default Pdfmedia;
