import React, { Component } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import firebaseInit from "../../config/firebaseInit";

import NavigationBar from "../../components/Navigationbar";
import Footer from "../../components/Footer";

import style from "../../assets/styles/UserProfile.module.css";

const db = getDatabase(firebaseInit);

export class UserStory extends Component {
  constructor() {
    super();
    this.state = {
      userData: [],
    };
  }

  componentDidMount() {
    const dbRef = ref(db, "userstory");
    onValue(dbRef, (snapshot) => {
      const record = [];
      snapshot.forEach((childSnapshot) => {
        const keyName = childSnapshot.key;
        const data = childSnapshot.val();
        record.push({ key: keyName, data });
      });

      this.setState({ userData: record });
    });
  }

  render() {
    return (
      <div>
        <NavigationBar />
        <h3
          style={{
            textAlign: "center",
            padding: "30px",
            background: "purple",
          }}
        >
          UserStory
        </h3>
        <div className={style.table_wrapper}>
          <table
            className={style.f1_table}
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              border: "1px solid white",
              marginBottom: "150px",
              background: "black",
            }}
          >
            <thead>
              <tr>
                <th style={{ padding: "7px" }}>#</th>
                <th style={{ padding: "7px" }}>username</th>
                <th style={{ padding: "7px" }}>Win</th>
                <th style={{ padding: "7px" }}>Draw</th>
                <th style={{ padding: "7px" }}>Lose</th>
              </tr>
            </thead>
            <tbody>
              {this.state.userData.map((row, index) => {
                return (
                  <>
                    <tr>
                      <td style={{ padding: "7px" }}>{index}</td>
                      <td style={{ padding: "7px" }}>{row.data.username}</td>
                      <td style={{ padding: "7px" }}>{row.data.win}</td>
                      <td style={{ padding: "7px" }}>{row.data.draw}</td>
                      <td style={{ padding: "7px" }}>{row.data.lose}</td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </table>
        </div>
        <Footer />
      </div>
    );
  }
}
export default UserStory;
