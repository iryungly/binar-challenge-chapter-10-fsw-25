import Image from "next/image";
import Footer from "../../components/Footer";
import NavigationBar from "../../components/Navigationbar";

export default function feature() {
  return (
    <>
      <NavigationBar />
      <div>
        <h1 style={{ padding: "30px", textAlign: "center" }}>Features</h1>
        <div style={{ textAlign: "center" }}>
          <Image
            src={"/feature_1.jpg"}
            width={1000}
            height={544}
            style={{
              marginBottom: "40px",
            }}
            alt=""
          />
          <Image
            src={"/feature_2.jpg"}
            width={1000}
            height={544}
            style={{
              marginBottom: "40px",
            }}
            alt=""
          />
          <Image
            src={"/feature_3.jpg"}
            width={1000}
            height={544}
            style={{
              marginBottom: "40px",
            }}
            alt=""
          />
          <Image
            src={"/feature_4.jpg"}
            width={1000}
            height={544}
            style={{
              marginBottom: "40px",
            }}
            alt=""
          />
          <Image
            src={"/feature_5.jpg"}
            width={1000}
            height={544}
            style={{
              marginBottom: "150px",
            }}
            alt=""
          />
        </div>
      </div>
      <Footer />
    </>
  );
}
