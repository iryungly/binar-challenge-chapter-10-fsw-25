import React from "react";
import Image from "next/image";
import Link from "next/link";

import NavigationBar from "../../components/Navigationbar";
import Footer from "../../components/Footer";

const Badge = ({ Component, pageProps }) => {
  return (
    <>
      <NavigationBar />
      <Link href="/gamelist">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          style={{ verticalAlign: "-0.125em", margin: "30px" }}
          width="40px"
          height="40px"
          preserveAspectRatio="xMidYMid meet"
          viewBox="0 0 512 512"
        >
          <path
            fill="white"
            d="m273.77 169.57l-89.09 74.13a16 16 0 0 0 0 24.6l89.09 74.13A16 16 0 0 0 300 330.14V181.86a16 16 0 0 0-26.23-12.29Z"
          />
          <path
            fill="none"
            stroke="white"
            stroke-miterlimit="10"
            stroke-width="32"
            d="M448 256c0-106-86-192-192-192S64 150 64 256s86 192 192 192s192-86 192-192Z"
          />
        </svg>
      </Link>

      <div className="grid-element">
        <h1 style={{ textAlign: "center", padding: "30px" }}>
          {" "}
          BADGE ACHIEVEMENT
        </h1>
        <Image
          src={require("../../public/image/badge.png")}
          width={1150}
          height={850}
          alt=""
          style={{ marginLeft: "60px", marginBottom: "150px" }}
        />
      </div>
      <Footer />
    </>
  );
};

export default Badge;
