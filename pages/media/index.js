import NavBar from "../../components/Navigationbar";
import React, { useState } from "react";
import ReactPlayer from "react-player";
import styles from "../../assets/styles/media.module.css";

const Media = () => {
  const [videoUrl, setVideoUrl] = useState("");
  const [audioUrl, setAudioUrl] = useState("");

  return (
    <>
      <NavBar />
      <div className={styles.playerContainer}>
        <h1 className={styles.title}> Media player </h1>
        <p className={styles.text}>
          {" "}
          This is a media player, you can put link of a video and put it here to
          play it, also can play a video from youtube{" "}
        </p>
        <input
          type="text"
          placeholder="Enter video link"
          onChange={(e) => setVideoUrl(e.target.value)}
          className={styles.input}
        />
        <button
          onClick={() => {
            /* play video using videoUrl */
          }}
          className={styles.button}
        >
          Play Video
        </button>
        <ReactPlayer url={videoUrl} controls className={styles.reactPlayer} />
        <p className={styles.text}>
          {" "}
          and below here is a audio player, just add a link of a audio you wanna
          play it will be played below{" "}
        </p>
        <input
          type="text"
          placeholder="Enter audio link"
          onChange={(e) => setAudioUrl(e.target.value)}
          className={styles.input}
        />
        <button
          onClick={() => {
            /* play audio using audioUrl */
          }}
          className={styles.button}
        >
          Play Audio
        </button>
        <audio src={audioUrl} controls className={styles.audio} />
      </div>
    </>
  );
};
export default Media;

// https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba.mp3
