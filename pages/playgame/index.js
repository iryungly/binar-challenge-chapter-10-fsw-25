import React from "react";
import NavBar from "../../components/Navigationbar";

const Playgame = () => {
  return (
    <>
      <NavBar />
      <div className="Playgame">
        <iframe
          id="serviceFrameSend"
          title="1"
          src="https://playgamessylv.000webhostapp.com"
          width="1350"
          height="700"
          frameborder="0"
        ></iframe>
      </div>
    </>
  );
};
export default Playgame;
