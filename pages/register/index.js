import { useState } from "react";
import Swal from "sweetalert2";
import firebaseInit, { auth } from "../../config/firebaseInit";

import { createUserWithEmailAndPassword } from "firebase/auth";
import { useRouter } from "next/router";
import Link from "next/link";
// import NavBar from '/components/Navigationbar';
import { getDatabase, ref, set, push } from "firebase/database";
import { last } from "lodash";

const Register = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [uid, setUid] = useState("");
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [datebirth, setDateBirth] = useState("");
  const [gender, setGender] = useState("");
  const router = useRouter();

  const writeUserData = (
    userId,
    first_name,
    last_name,
    email,
    datebirth,
    gender
  ) => {
    const db = getDatabase(firebaseInit);
    const param = {
      first_name,
      last_name,
      email,
      uid: userId,
      gender,
      datebirth,
      score: Math.floor(Math.random() * 100),
      username: email,
      status: "Played",
      played: true,
    };
    console.log(param, "param");
    set(ref(db, "users/" + userId), param);
  };
  const setTglLahir = (value) => {
    console.log(value);
    setDateBirth(value);
  };
  const registerHandler = async (e) => {
    console.log("register");
    e.preventDefault();

    // return;
    createUserWithEmailAndPassword(auth, email, password)
      .then((hsl) => {
        console.log(hsl);
        setUid(hsl.user.uid);
        writeUserData(
          hsl.user.uid,
          first_name,
          last_name,
          email,
          datebirth,
          gender
        );
        Swal.fire({
          icon: "success",
          text: "Register berhasil",
        }).then((hsl) => {
          router.push("/login");
        });
      })
      .catch((err) => {
        console.log(err);
        Swal.fire({
          icon: "error",
          text: "Register gagal " + err.message,
        });
      });
  };
  return (
    <>
      <section className="vw-100 vh-100" style={{ background: "purple" }}>
        <div className="container d-flex justify-content-center align-items-center">
          <form>
            <h3
              className="text-center md-3 p-5"
              style={{ fontFamily: "arial black", color: "plum" }}
            >
              Sign Up
            </h3>
            <div className="row">
              <div className="col-md-6">
                <div className="mb-3" style={{ color: "purple" }}>
                  <label className="form-label" style={{ color: "white" }}>
                    First Name :
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter first name"
                    value={first_name}
                    onChange={(e) => setFirstName(e.target.value)}
                    required
                  />
                </div>
                <div className="mb-3" style={{ color: "purple" }}>
                  <label className="form-label" style={{ color: "white" }}>
                    Last Name :
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter last name"
                    value={last_name}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                </div>
                <div className="mb-3" style={{ color: "purple" }}>
                  <label className="form-label" style={{ color: "white" }}>
                    Date of Birth :
                  </label>
                  <input
                    type="date"
                    className="form-control"
                    placeholder="Enter last name"
                    value={datebirth}
                    onChange={(e) => setTglLahir(e.target.value)}
                  />
                </div>
              </div>
              <div className="col-md-6">
                <div className="mb-3" style={{ color: "purple" }}>
                  <label className="form-label" style={{ color: "white" }}>
                    Gender :
                  </label>
                  <select
                    className="form-control"
                    onChange={(e) => setGender(e.target.value)}
                    required
                  >
                    <option value="">Choose</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                </div>
                <div className="mb-3" style={{ color: "purple" }}>
                  <label className="form-label" style={{ color: "white" }}>
                    Email address: :
                  </label>
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </div>
                <div className="mb-3" style={{ color: "purple" }}>
                  <label className="form-label" style={{ color: "white" }}>
                    Password: :
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Enter password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </div>
              </div>
            </div>
            <div className="text-center md-3 p-4">
              <button
                type="submit"
                className="btn btn-primary"
                style={{ backgroundColor: "plum" }}
                onClick={registerHandler}
              >
                Sign Up
              </button>
            </div>
            <p className="forgot-password text-center ">
              Already registered? <Link href="/login">Sign in</Link>
            </p>
          </form>
        </div>
      </section>
    </>
  );
};

export default Register;
