import React, { Component } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import app from "../../config/firebaseInit";

import style from "../../assets/styles/UserProfile.module.css";
import NavigationBar from "../../components/Navigationbar";
import Footer from "../../components/Footer";

const db = getDatabase(app);

class userprofile extends Component {
  constructor() {
    super();
    this.state = {
      userData: [],
    };
  }

  componentDidMount() {
    const dbRef = ref(db, "users");
    onValue(dbRef, (snapshot) => {
      const record = [];
      snapshot.forEach((childSnapshot) => {
        const keyName = childSnapshot.key;
        const data = childSnapshot.val();
        record.push({ key: keyName, data });
      });

      this.setState({ userData: record });
    });
  }

  render() {
    return (
      <div>
        <NavigationBar />
        <h1
          className={style.title}
          style={{
            textAlign: "center",
            padding: "30px",
            backgroundColor: "purple",
          }}
        >
          UserProfile
        </h1>
        <div className={style.table_wrapper}>
          <table
            className={style.f1_table}
            style={{
              marginLeft: "auto",
              marginRight: "auto",
              border: "1px solid white",
              marginBottom: "150px",
              backgroundColor: "black",
            }}
          >
            <thead className={style.thead}>
              <tr>
                <th className={style.th} style={{ padding: "15px" }}>
                  #
                </th>
                <th className={style.th} style={{ padding: "15px" }}>
                  first_name
                </th>
                <th className={style.th} style={{ padding: "15px" }}>
                  last_name
                </th>
                <th className={style.th} style={{ padding: "15px" }}>
                  username
                </th>
                <th className={style.th} style={{ padding: "15px" }}>
                  email
                </th>
                <th className={style.th} style={{ padding: "15px" }}>
                  date birth
                </th>
                <th className={style.th} style={{ padding: "15px" }}>
                  gender
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.userData.map((row, index) => {
                return (
                  <>
                    <tr>
                      <td className={style.td} style={{ padding: "15px" }}>
                        {index}
                      </td>
                      <td className={style.td} style={{ padding: "15px" }}>
                        {row.data.first_name}
                      </td>
                      <td className={style.td} style={{ padding: "15px" }}>
                        {row.data.last_name}
                      </td>
                      <td className={style.td} style={{ padding: "15px" }}>
                        {row.data.username}
                      </td>
                      <td className={style.td} style={{ padding: "15px" }}>
                        {row.data.email}
                      </td>
                      <td className={style.td} style={{ padding: "15px" }}>
                        {row.data.datebirth}
                      </td>
                      <td className={style.td} style={{ padding: "15px" }}>
                        {row.data.gender}
                      </td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </table>
        </div>
        <Footer />
      </div>
    );
  }
}

export default userprofile;
