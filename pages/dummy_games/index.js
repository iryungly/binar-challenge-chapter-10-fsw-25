import React, { Component, useState } from "react";
import style from "../../assets/styles/Dummy_games.module.css";

import Footer from "../../components/Footer";
import NavigationBar from "../../components/Navigationbar";

import { connect } from "react-redux";

class dummy_games extends Component {
  render() {
    return (
      <div>
        <NavigationBar />
        <div className={style.body}>
          <h1 className={style.title}>DummyGames</h1>
          <div className={style.container}>
            <div className={style.randomNum}>
              <h3 className={style.h3}>Random Score</h3>
              <h1 suppressHydrationWarning className={style.h1}>
                {this.props.dummy}
              </h1>
            </div>

            <button
              className={style.button_28}
              onClick={this.props.changeScore}
            >
              Get random number
            </button>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dummy: state.dummy,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeScore: () => dispatch({ type: "CHANGE_SCORE" }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(dummy_games);
