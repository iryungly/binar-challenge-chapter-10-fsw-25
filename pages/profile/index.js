import Footer from "../../components/Footer";
import NavigationBar from "../../components/Navigationbar";

const profile = () => {
  return (
    <>
      <NavigationBar />
      <form>
        <h3>profile</h3>
        <div className="mb-3">
          <label>First name</label>
          <input
            type="text"
            className="form-control"
            placeholder="First name"
          />
        </div>
        <div className="mb-3">
          <label>Last name</label>
          <input type="text" className="form-control" placeholder="Last name" />
        </div>
        <div className="mb-3">
          <label>Email address</label>
          <input
            type="email"
            className="form-control"
            placeholder="Enter email"
          />
        </div>
        <div className="mb-3">
          <label>Password</label>
          <input
            type="password"
            className="form-control"
            placeholder="Enter password"
          />
        </div>
        <div className="d-grid">
          <button type="submit" className="btn btn-primary">
            Sign Up
          </button>
        </div>
        <p className="forgot-password text-center">
          Already registered <a href="/sign-in">sign in?</a>
        </p>
      </form>
      <Footer />
    </>
  );
};

export default profile;
