import React from "react";
import Image from "next/image";
import Link from "next/link";
import NavBar from "../../components/Navigationbar";

const Game = () => {
  return (
    <>
      <NavBar />
      <section classNameName="main-content" style={{ color: "purple" }}>
        <div className="container">
          <h1 className="text-center">
            Our <b>Team</b>
          </h1>
          <p className="text-center text-muted">Binar FSW 25</p>
          <br></br>
          <div className="row">
            <div
              class="col-lg-4 col-md-6 col-sm-12 mb-4"
              style={{ color: "black" }}
            >
              <div class="profile-card bg-white shadow mb-4 text-center rounded-lg p-4 position-relative h-60">
                <div class="profile-card_image">
                  <Image
                    src={require("../../public/images/users/user1.png")}
                    width={175}
                    height={105}
                    alt=""
                  />
                </div>
                <div class="profile-card_details">
                  <h3 className="mb-0">Sylvia Listyani</h3>
                  <p className="text-muted">Scrum Master</p>
                  <p className="text-muted"></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
              <div class="profile-card bg-white shadow mb-4 text-center rounded-lg p-4 position-relative h-60">
                <div class="profile-card_image">
                  <Image
                    src={require("../../public/images/users/user2.jpg")}
                    width={175}
                    height={105}
                    alt=""
                  />
                </div>
                <div class="profile-card_details">
                  <h3 className="mb-0">Fauzi Fauzi</h3>
                  <p className="text-muted">Repo Builder & Maintenance</p>
                  <p className="text-muted"></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
              <div class="profile-card bg-white shadow mb-4 text-center rounded-lg p-4 position-relative h-60">
                <div class="profile-card_image">
                  <Image
                    src={require("../../public/images/users/user3.jpg")}
                    width={175}
                    height={105}
                    alt=""
                  />
                </div>
                <div class="profile-card_details">
                  <h3 className="mb-0"> Akmal Faturahman</h3>
                  <p className="text-muted">Boiler Plate 1</p>
                  <p className="text-muted"></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
              <div class="profile-card bg-white shadow mb-4 text-center rounded-lg p-4 position-relative h-60">
                <div class="profile-card_image">
                  <Image
                    src={require("../../public/images/users/user7.jpg")}
                    width={175}
                    height={105}
                    alt=""
                  />
                </div>
                <div class="profile-card_details">
                  <h3 className="mb-0">Suhairoh</h3>
                  <p className="text-muted">Boiler Plate 2</p>
                  <p className="text-muted"></p>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
              <div class="profile-card bg-white shadow mb-4 text-center rounded-lg p-4 position-relative h-60">
                <div class="profile-card_image">
                  <Image
                    src={require("../../public/images/users/user6.jpg")}
                    width={175}
                    height={105}
                    alt=""
                  />
                </div>
                <div class="profile-card_details">
                  <h3 className="mb-0">Andri</h3>
                  <p className="text-muted">DevOps</p>
                  <p className="text-muted"></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
              <div class="profile-card bg-white shadow mb-4 text-center rounded-lg p-4 position-relative h-60">
                <div class="profile-card_image">
                  <Image
                    src={require("../../public/images/users/user5.png")}
                    width={175}
                    height={105}
                    alt=""
                  />
                </div>
                <div class="profile-card_details">
                  <h3 className="mb-0">Fauzan</h3>
                  <p className="text-muted">Repo Maintener</p>
                  <p className="text-muted"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Game;
